title: Development
description: Simple, universally, fast. This is how the basic structure of the project should arise.

# Development <small>How?</small>

Simple, universally, fast. This is how the basic structure of the project should arise.

In this repository I would like to include all universal patterns for quick development of plugins and templates for WordPress and OpenCart, along with simple but very clear documentation, fast and stable local, staging and production deployment using Docker and Kubernates.
