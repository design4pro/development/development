# Development

[![buddy pipeline](https://app.buddy.works/design4pro/development/pipelines/pipeline/69797/badge.svg?token=08c933fbd4a9ca6c7f73de6feef6aa3f74989f4ced597af768b218c1e3878e0a "buddy pipeline")](https://app.buddy.works/design4pro/development/pipelines/pipeline/69797)

Base structure for projects.

See https://design4pro.github.io/development
