variable "billing_account" {
  description = "This is the GPC billing account name"
}

variable "org_id" {
  description = "This is the GPC organization name"
}

variable "project_name" {
  description = "This is the GPC project name"
}

variable "region" {
  description = "This is the GPC project region"
  default = "europe-west3"
}
