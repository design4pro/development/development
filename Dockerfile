FROM alpine/nginx:latest

LABEL maintainer="r.wolak@design4.pro"

RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
  ln -sf /dev/stderr /var/log/nginx/error.log

RUN apk update && apk upgrade

# install ca-certificates so that HTTPS works consistently # the other runtime dependencies for Python are installed later
RUN apk add --no-cache ca-certificates

# Install Python
RUN apk add --update \
  python \
  python-dev \
  py-pip \
  build-base \
  && rm -rf /var/cache/apk/*

COPY . /usr/share/nginx

WORKDIR /usr/share/nginx

# Install
RUN pip install -r requirements.txt

# Build
RUN mkdocs build --site-dir html

